package main

import (
	"os"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func setupDB() {

	datastoreName := os.Getenv("DATABASE_URL")

	db, err = sqlx.Open("postgres", datastoreName)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}
}
