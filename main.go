package main

import (
	"fmt"

	_ "github.com/heroku/x/hmetrics/onload"
)

func main() {
	fmt.Println("Bring the rain...")
	setupRayGun()
	setupRouter()
}
