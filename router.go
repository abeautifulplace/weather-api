package main

import "github.com/gin-gonic/gin"

func setupRouter() {
	router := gin.Default()

	router.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"code": "PAGE_NOT_FOUND", "message": "Page not found"})
	})

	api := router.Group("/api")

	api.GET("/ping", pingEndPoint)
	api.HEAD("/ping", pingEndPoint)

	api.POST("/location", locationEndPoint)

	router.Run()
}
