package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func pingEndPoint(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "pong"})
}

func locationEndPoint(c *gin.Context) {
	lat := c.Query("lat")
	lon := c.Query("lon")
	result := getForecast(lat, lon)
	c.String(http.StatusOK, result)
}
