package main

import "github.com/jmoiron/sqlx"

var (
	db  *sqlx.DB
	err error
)

const (
	hostname = "http://api.openweathermap.org/data/2.5"
	appid    = "758fc36dff4f212868d3fb7149d8f9ff"
	lang     = "en"
	units    = "metric"
)
