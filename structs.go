package main

type Location struct {
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	Visibility int       `json:"visibility"`
	Main       Main      `json:"main"`
	Clouds     Clouds    `json:"clouds"`
	Weather    []Weather `json:"weather"`
	Wind       Wind      `json:"wind"`
	Sys        Sys       `json:"sys"`
}

type Main struct {
	Temp     float32 `json:"temp"`
	Pressure int     `json:"pressure"`
	Humidity int     `json:"humidity"`
	TempMin  int     `json:"temp_min"`
	TempMax  int     `json:"temp_max"`
}

type Weather struct {
	Description string `json:"description"`
	Code        int    `json:"id"`
}

type Clouds struct {
	All int `json:"all"`
}

type Wind struct {
	Deg   int     `json:"deg"`
	Speed float32 `json:"speed"`
}

type Sys struct {
	Sunrise int `json:"sunrise"`
	Sunset  int `json:"sunset"`
}

type Forecast struct {
	ID                 int     `json:"id"`
	Name               string  `json:"name"`
	Pressure           int     `json:"pressure"`
	Humidity           int     `json:"humidity"`
	Temp               float32 `json:"temp"`
	TempMin            int     `json:"temp_min"`
	TempMax            int     `json:"temp_max"`
	Clouds             int     `json:"clouds"`
	Sunrise            int     `json:"sunrise"`
	Sunset             int     `json:"sunset"`
	Visibility         int     `json:"visibility"`
	WeatherCode        int     `json:"weather_code"`
	WeatherDescription string  `json:"weather_description"`
	WindDegree         int     `json:"wind_degree"`
	WindSpeed          float32 `json:"wind_speed"`
}
