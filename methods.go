package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-redis/redis"
)

func getForecast(lat string, lon string) string {
	setupDB()

	redisName := os.Getenv("REDIS_URL")
	opt, _ := redis.ParseURL(redisName)
	redisClient := redis.NewClient(opt)

	var cityID string

	sqlStatement := `SELECT id FROM cities ORDER BY geom <-> st_setsrid(st_makepoint($1, $2),4326) LIMIT 1;`
	err = db.Get(&cityID, sqlStatement, lat, lon)
	defer db.Close()

	result, err := redisClient.Get(cityID).Result()
	if err == redis.Nil {
		url := fmt.Sprintf("%s/weather?id=%s&lang=%s&appid=%s&units=%s", hostname, cityID, lang, appid, units)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Println(err)
			return "{'message': 'error'}"
		}

		client := &http.Client{}

		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			return "{'message': 'error'}"
		}

		var location Location

		if err := json.NewDecoder(resp.Body).Decode(&location); err != nil {
			log.Println(err)
			return "{'message': 'error'}"
		}

		defer resp.Body.Close()

		forecast := Forecast{}

		forecast.ID = location.ID
		forecast.Name = location.Name
		forecast.Humidity = location.Main.Humidity
		forecast.Pressure = location.Main.Pressure
		forecast.Temp = location.Main.Temp
		forecast.TempMax = location.Main.TempMax
		forecast.TempMin = location.Main.TempMin
		forecast.Clouds = location.Clouds.All
		forecast.Sunrise = location.Sys.Sunrise
		forecast.Sunset = location.Sys.Sunset
		forecast.Visibility = location.Visibility
		forecast.WeatherCode = location.Weather[0].Code
		forecast.WeatherDescription = location.Weather[0].Description
		forecast.WindDegree = location.Wind.Deg
		forecast.WindSpeed = location.Wind.Speed

		result, _ := json.Marshal(forecast)

		err = redisClient.Set(cityID, result, time.Second*300).Err()
		if err != nil {
			log.Println(err)
			return "{'message': 'error'}"
		}

		return string(result)
	} else if err != nil {
		panic(err)
	} else {
		return result
	}

}
