package main

import (
	"log"

	"github.com/MindscapeHQ/raygun4go"
)

func setupRayGun() {
	raygun, err := raygun4go.New("ForecastApp", "0ZtYa6nEhiCwrD0nsKGPA")
	if err != nil {
		log.Println("Unable to create Raygun client:", err.Error())
	}
	defer raygun.HandleError()
}
